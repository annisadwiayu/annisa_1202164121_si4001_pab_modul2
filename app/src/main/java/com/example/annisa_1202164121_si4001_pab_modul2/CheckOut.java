package com.example.annisa_1202164121_si4001_pab_modul2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class CheckOut extends AppCompatActivity {

    TextView tvTanggalBerangkat, tvTanggalPulang, tvJumlahTiket, tvHargaTotal, tvTujuan, tvPulang;
    Button btnKonfirmasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);

        tvTujuan = findViewById(R.id.tv_tujuan_summary);
        tvTanggalBerangkat = findViewById(R.id.tv_tanggal_berangkat_summary);
        tvTanggalPulang = findViewById(R.id.tv_tanggal_pulang_summary);
        tvPulang = findViewById(R.id.tvPulang);
        tvJumlahTiket = findViewById(R.id.tv_jumlah_tiket_summary);
        tvHargaTotal = findViewById(R.id.tv_harga_total_summary);
        btnKonfirmasi = findViewById(R.id.btn_konfirmasi);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            String tujuan, tanggalBerangkat, tanggalPulang, jumlahTiket, hargaTotal;
            tujuan = extras.getString("tujuan");
            tanggalBerangkat = extras.getString("tanggalBerangkat");
            tanggalPulang = extras.getString("tanggalPulang");
            jumlahTiket = extras.getString("jumlahTiket");
            hargaTotal = extras.getString("hargaTotal");
            tvTanggalPulang.setVisibility(View.VISIBLE);
            tvPulang.setVisibility(View.VISIBLE);
            tvTujuan.setText(tujuan);
            tvTanggalBerangkat.setText(tanggalBerangkat);
            tvTanggalPulang.setText(tanggalPulang);
            tvJumlahTiket.setText(jumlahTiket);
            tvHargaTotal.setText(hargaTotal);
        }
    }

    public void konfirm(View view) {
        Intent done = new Intent(CheckOut.this, MainActivity.class);
        Toast.makeText(this,"Terima Kasih! Semoga Selamat Sampai Tujuan",Toast.LENGTH_LONG).show();
        startActivity(done);
    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.d("PAB/" + this.getClass().getSimpleName(), "ini onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("PAB/" + this.getClass().getSimpleName(), "ini onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("PAB/" + this.getClass().getSimpleName(), "ini onPause");
    }
}


